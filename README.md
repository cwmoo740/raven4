# raven4

An AI agent that attempts to solve Raven's Progressive Matrices problems using only image processing on each frame.
The important processing all takes place in Agent.py, and all of the setup code in Problem4.py.
Running Problem4.py loads image data from the /Problems (Image Data) folder, and supplies new problems to the agent
and logs the results.
